-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 12 Novembre 2020 à 18:19
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `corralesp2_vlib`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonne`
--

CREATE TABLE IF NOT EXISTS `abonne` (
  `codeAcces` int(5) NOT NULL,
  `codeSecret` char(5) NOT NULL,
  `codeAbo` int(10) NOT NULL,
  `nom` char(32) DEFAULT NULL,
  `prenom` char(32) DEFAULT NULL,
  `dateDebAbo` date DEFAULT NULL,
  `dateFinAbo` date DEFAULT NULL,
  `creditTemps` time DEFAULT NULL,
  `montantADebiter` decimal(10,2) DEFAULT NULL,
  `typeUtil` char(32) DEFAULT NULL,
  PRIMARY KEY (`codeAcces`,`codeSecret`),
  KEY `I_FK_ABONNE_ABONNEMENT` (`codeA`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `abonne`
--

INSERT INTO `abonne` (`codeAcces`, `codeSecret`, `codeA`, `nom`, `prenom`, `dateDebAbo`, `dateFinAbo`, `creditTemps`, `montantADebiter`, `typeUtil`) VALUES
(123, '123', 4, 'Corrales', 'Pablo', '2020-10-01', '2021-10-01', '00:00:30', NULL, 'abonne'),
(419, '419', 5, 'Derives', 'Geralt', NULL, NULL, NULL, NULL, 'responsable');

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

CREATE TABLE IF NOT EXISTS `abonnement` (
  `codeAbo` int(10) NOT NULL,
  `libelleAbo` char(32) DEFAULT NULL,
  `dureeAbo` int(5) DEFAULT NULL,
  `montantAbo` decimal(10,2) DEFAULT NULL,
  `creditTempsBase` time DEFAULT NULL,
  `tarifHoraire` decimal(10,2) DEFAULT NULL,
  `caution` decimal(10,2) DEFAULT NULL,
  `typeA` int(1) DEFAULT NULL,
  PRIMARY KEY (`codeAbo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `abonnement`
--

INSERT INTO `abonnement` (`codeAbo`, `libelleAbo`, `dureeAbo`, `montantAbo`, `creditTempsBase`, `tarifHoraire`, `caution`, `typeA`) VALUES
(1, 'Abonnement 24h', 1, '1.50', '00:30:00', '2.00', '200.00', 1),
(2, 'Abonnement 7j', 7, '7.00', '00:30:00', '2.00', '200.00', 1),
(3, 'Abonnement 1 mois', 30, '10.00', '00:30:00', '1.00', '200.00', 2),
(4, 'Abonnement 1 an', 365, '30.00', '00:30:00', '1.00', '200.00', 2),
(5, 'sans abonnement', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE IF NOT EXISTS `emprunt` (
  `codeAcces` int(5) NOT NULL,
  `codeSecret` char(5) NOT NULL,
  `numV` int(2) NOT NULL,
  `dateHeure` datetime NOT NULL,
  `tempsLoc` char(32) DEFAULT NULL,
  PRIMARY KEY (`codeAcces`,`codeSecret`,`numV`,`dateHeure`),
  KEY `I_FK_EMPRUNT_ABONNE` (`codeAcces`,`codeSecret`),
  KEY `I_FK_EMPRUNT_VELO` (`numV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `etatplot`
--

CREATE TABLE IF NOT EXISTS `etatplot` (
  `dateHeure` datetime NOT NULL,
  `numS` int(5) NOT NULL,
  `numP` int(5) NOT NULL,
  `etatP` char(32) DEFAULT NULL,
  `duree` char(32) DEFAULT NULL,
  PRIMARY KEY (`dateHeure`,`numS`,`numP`),
  KEY `I_FK_ETATPLOT_PLOT` (`numS`,`numP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `etatplot`
--

INSERT INTO `etatplot` (`dateHeure`, `numS`, `numP`, `etatP`, `duree`) VALUES
('2020-11-12 18:12:00', 1, 1, 'maintenance', '01:14');

-- --------------------------------------------------------

--
-- Structure de la table `etatstation`
--

CREATE TABLE IF NOT EXISTS `etatstation` (
  `dateHeure` datetime NOT NULL,
  `numS` int(5) NOT NULL,
  `etatS` char(32) DEFAULT NULL,
  `duree` time DEFAULT NULL,
  PRIMARY KEY (`dateHeure`,`numS`),
  KEY `I_FK_ETATSTATION_STATION` (`numS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `etatstation`
--

INSERT INTO `etatstation` (`dateHeure`, `numS`, `etatS`, `duree`) VALUES
('2020-11-11 16:00:00', 1, 'fonctionnel', '00:00:01'),
('2020-11-12 17:51:00', 1, 'maintenance', '02:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `etatvelo`
--

CREATE TABLE IF NOT EXISTS `etatvelo` (
  `dateHeure` datetime NOT NULL,
  `numV` int(2) NOT NULL,
  `etatP` char(32) DEFAULT NULL,
  `duree` time DEFAULT NULL,
  PRIMARY KEY (`dateHeure`,`numV`),
  KEY `I_FK_ETATVELO_VELO` (`numV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `etatvelo`
--

INSERT INTO `etatvelo` (`dateHeure`, `numV`, `etatP`, `duree`) VALUES
('2020-11-12 18:01:00', 1, 'perdu', '05:30:00');

-- --------------------------------------------------------

--
-- Structure de la table `plot`
--

CREATE TABLE IF NOT EXISTS `plot` (
  `numS` int(5) NOT NULL,
  `numP` int(5) NOT NULL,
  `numV` int(2) DEFAULT NULL,
  `etat` char(32) DEFAULT NULL,
  PRIMARY KEY (`numS`,`numP`),
  UNIQUE KEY `I_FK_PLOT_VELO` (`numV`),
  KEY `I_FK_PLOT_STATION` (`numS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `plot`
--

INSERT INTO `plot` (`numS`, `numP`, `numV`, `etat`) VALUES
(1, 1, 1, 'fonctionnel'),
(1, 2, 2, 'fonctionnel'),
(2, 3, 3, 'fonctionnel');

-- --------------------------------------------------------

--
-- Structure de la table `station`
--

CREATE TABLE IF NOT EXISTS `station` (
  `numS` int(5) NOT NULL,
  `etatS` char(32) DEFAULT NULL,
  `nomS` char(32) DEFAULT NULL,
  `situationS` varchar(128) DEFAULT NULL,
  `capaciteS` char(32) DEFAULT NULL,
  `numBorne` int(5) DEFAULT NULL,
  PRIMARY KEY (`numS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `station`
--

INSERT INTO `station` (`numS`, `etatS`, `nomS`, `situationS`, `capaciteS`, `numBorne`) VALUES
(1, 'en maintenance', 'velo01', 'bordeaux', '10', 1),
(2, 'fonctionnel', 'velo02', 'bordeaux', '15', 2),
(3, 'en maintenance', 'velo03', 'bordeaux', '13', 3);

-- --------------------------------------------------------

--
-- Structure de la table `velo`
--

CREATE TABLE IF NOT EXISTS `velo` (
  `numV` int(2) NOT NULL,
  `numS` int(5) DEFAULT NULL,
  `numP` int(5) DEFAULT NULL,
  `etatV` char(32) DEFAULT NULL,
  `DMEC` char(32) DEFAULT NULL,
  PRIMARY KEY (`numV`),
  UNIQUE KEY `I_FK_VELO_PLOT` (`numS`,`numP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `velo`
--

INSERT INTO `velo` (`numV`, `numS`, `numP`, `etatV`, `DMEC`) VALUES
(1, 1, 1, 'en maintenance', 'dmec'),
(2, 1, 2, 'fonctionnel', 'dmec'),
(3, 2, 3, 'fonctionnel', 'dmec');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `abonne`
--
ALTER TABLE `abonne`
  ADD CONSTRAINT `FK_ABONNE_ABONNEMENT` FOREIGN KEY (`codeAbo`) REFERENCES `abonnement` (`codeAbo`);

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `FK_EMPRUNT_ABONNE` FOREIGN KEY (`codeAcces`, `codeSecret`) REFERENCES `abonne` (`codeAcces`, `codeSecret`),
  ADD CONSTRAINT `FK_EMPRUNT_VELO` FOREIGN KEY (`numV`) REFERENCES `velo` (`numV`);

--
-- Contraintes pour la table `etatplot`
--
ALTER TABLE `etatplot`
  ADD CONSTRAINT `FK_ETATPLOT_PLOT` FOREIGN KEY (`numS`, `numP`) REFERENCES `plot` (`numS`, `numP`);

--
-- Contraintes pour la table `etatstation`
--
ALTER TABLE `etatstation`
  ADD CONSTRAINT `FK_ETATSTATION_STATION` FOREIGN KEY (`numS`) REFERENCES `station` (`numS`);

--
-- Contraintes pour la table `etatvelo`
--
ALTER TABLE `etatvelo`
  ADD CONSTRAINT `FK_ETATVELO_VELO` FOREIGN KEY (`numV`) REFERENCES `velo` (`numV`);

--
-- Contraintes pour la table `plot`
--
ALTER TABLE `plot`
  ADD CONSTRAINT `FK_PLOT_STATION` FOREIGN KEY (`numS`) REFERENCES `station` (`numS`),
  ADD CONSTRAINT `FK_PLOT_VELO` FOREIGN KEY (`numV`) REFERENCES `velo` (`numV`);

--
-- Contraintes pour la table `velo`
--
ALTER TABLE `velo`
  ADD CONSTRAINT `FK_VELO_PLOT` FOREIGN KEY (`numS`, `numP`) REFERENCES `plot` (`numS`, `numP`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
