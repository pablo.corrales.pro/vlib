<?php require_once 'lib/autoLoader.php';
session_start();?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<title>VLib</title>
		<style type="text/css">
			@import url(style/vlib.css);
		</style>
	</head>
	<body>
		<?php
			require_once 'controleurs/controleurPrincipal.php';	
		?>
	</body>
	<footer>
	</footer>
</html>
