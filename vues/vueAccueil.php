<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
	<div class="accueil">
		<h1>Notre société SteSIO</h1>
		<h2>Qui sommes–nous ?</h2>

		<p>
		Nous délivrons des services qui s’appuient sur les nouvelles technologies et qui permettent aux
		administrations et aux entreprises d’être plus efficientes. Grâce à notre expertise des technologies de
		l’information, nous sommes en mesure de prendre en charge l’externalisation des processus métiers
		de nos entreprises clientes. Nous permettons donc à nos clients de se recentrer sur leur métier. Forts
		de nos cinq cents collaborateurs répartis essentiellement dans l’ouest de la France, nous prenons en
		charge les systèmes, les services et les processus qui facilitent la vie quotidienne de nos clients
		chaque jour.
		Créé en 1981, STESIO est présent partout en France. Nous avons réalisé un chiffre d’affaire de 150
		millions d’euros en 2010. Notre capital est détenu à hauteur de 10 % par nos collaborateurs. Notre
		siège social est basé à Nantes, et nous sommes cotés en bourse à Paris.
		</p>

		<h2>Nos activités</h2>

		<p>Nous accompagnons nos clients dans la création de nouveaux services ou l’exploitation de services
		existants. Nous nous engageons auprès de nos clients à créer de la valeur ajoutée, mesurable à
		travers nos contrats de service et cela pour un coût très compétitif. Nous pouvons intervenir sur tout le
		cycle de vie du système d’information de nos clients et accompagnons grâce à notre maîtrise de la
		technologie, leurs changements stratégiques, économiques et sociaux.
		Nous proposons une offre globale, allant de la conception à l’exploitation des systèmes d’information.
		Notre marché concerne aussi bien la très petite entreprise que les grands groupes auprès des
		desquels notre histoire et nos réalisations antérieures nous rendent crédibles. Nous intervenons dans
		les secteurs suivants : secteur public et santé, banque et assurance, télécommunications, énergietransport-
		industrie, nouvelles technologies. Nous utilisons aussi bien des solutions propriétaires que
		des solutions libres pour répondre aux besoins de nos clients.
		</p>

		<h2>Nos compétences</h2>

		<p>
		Nous maîtrisons des environnements technologiques très variés. Nous sommes capables de prendre
		en charge des projets requérant des modélisations entités associations ou des diagrammes de
		classes. Différentes méthodes de conception sont utilisés par nos différents collaborateurs en fonction
		des pôles où ils interviennent. Nous gérons aujourd’hui des projets de développements spécifiques en
		JAVA, en PHP, en C# ou en Python. Nous sommes capables aussi de mettre en place des progiciels
		de gestion intégrés. Nous nous adaptons sans cesse aux besoins de nos clients et nous comprenons
		leurs désirs de recourir aux logiciels libres pour la mise en place de leur infrastructure réseau, ce qui
		explique nos compétences en systèmes d’exploitation propriétaires comme libres.
		</p>

		<h2>Notre patrimoine informatique</h2>

		<p>
		La diversité des environnements technologiques et les délais de mise en oeuvre souvent très courts
		nous obligent à utiliser les techniques de virtualisation. Nous disposons de fermes de serveurs,
		certaines sous Hyper V de Microsoft et d’autres sous ESX de Vmware. Nos collaborateurs disposent
		de trois environnements : un environnement de développement, un environnement de recette et un
		environnement d’exploitation. L’utilisation de ces différents environnements dépend de la demande de
		nos clients et de leur capacité à héberger ou non leur système informatique. L’utilisation et la mise en
		place des environnements de développement et de recette à l’intérieur des fermes de serveurs
		relèvent des responsabilités de nos équipes de projet. La gestion de l’environnement d’exploitation est
		de la responsabilité de nos équipes systèmes et réseaux.
		Pour les clients hébergeant leur propre système informatique, nous disposons d’accès distants sécurisés permettant d’intervenir depuis nos locaux.
		</p>

		<h2>Nos processus</h2>

		<p>
		Comme toute entreprise, nous avons nos propres processus. Tout nouveau collaborateur doit
		respecter nos procédures de travail. Chaque équipe dispose d’un outil de travail collaboratif au sein
		duquel toute la documentation du projet qui lui confié doit être déposée .
		Nous sommes heureux de vous accueillir au sein de notre entreprise où vous pourrez exprimer tout
		votre talent, accompagné de solides formations.
		</p>
	</main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>