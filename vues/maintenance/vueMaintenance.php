<div class="conteneur">
	<header>
		<?php include 'vues/haut.php' ;?>
	</header>
	<main>
		<aside>
			<?php  include 'vues/maintenance/vueMaintenanceG.php' ;?> 
		</aside>
		<section>	
			<?php include 'vues/maintenance/vueMaintenanceD.php' ;?> 
		</section>
    </main>
	<footer>
		<?php include 'vues/bas.php' ;?>
	</footer>
</div>