<?php
spl_autoload_register('AutoLoader::autoLoadTrait');
spl_autoload_register('AutoLoader::autoLoadDto');
spl_autoload_register('AutoLoader::autoLoadDao');
spl_autoload_register('AutoLoader::autoLoadLib');

class AutoLoader{
    
    static function autoLoadTrait($class){
        $file = 'modeles/traits/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
        
    }
    
    static function autoLoadDto($class){
        $file = 'modeles/dto/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
      
    }
    
    static function autoLoadDao($class){
        $file = 'modeles/dao/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
        
    }

    static function autoLoadLib($class){
        $file = 'lib/' . lcfirst($class) . '.php';
        if(is_file($file)&& is_readable($file)){
            require $file;
        }
        
    }
}


