<?php
function console_log( $data ){
    echo '<script>';
    echo 'console.log('. json_encode( $data ) .')';
    echo '</script>';
}

if(isset($_GET['menuPrincipalVlib'])){
	$_SESSION['menuPrincipalVlib'] = $_GET['menuPrincipalVlib'];
}

else
{
	if(!isset($_SESSION['menuPrincipalVlib'])){
		$_SESSION['menuPrincipalVlib']="accueil";
	}
}

$menuPrincipalVlib = new Menu("menuPrincipalVlib");

$menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("accueil", "Accueil"));
$menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("abonnement", "Abonnement"));

$user = new Abonne();
$messageErreurConnexion = "";

if(isset($_POST['codeAcces'])){
    $user = new Abonne($_POST["codeAcces"], $_POST["codeSecret"]);
 
    $_SESSION['resultatVerif'] = AbonneDAO::verification($user);
    $_SESSION['infoAbo'] = AbonneDAO::infoAbo($user);

    if($_SESSION['resultatVerif']['codeAcces'] === $_POST['codeAcces']){
        $_SESSION['menuPrincipalVlib'] = "accueil";
    }
    else{
        $messageErreurConnexion = "Identifant ou mot de passe incorrect";
    }
}
if(isset($_SESSION['resultatVerif']) && $_SESSION['resultatVerif']){
    $type = $_SESSION["infoAbo"]["typeUtil"];
    $codeAbo = $_SESSION["infoAbo"]["codeA"];

    if($type === "usager"){
        if($codeAbo == "3" || $codeAbo == "4"){
            $menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("compte", "Mon Compte"));
        }
    }

    if($type === "responsable"){
        $menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("maintenance", "Maintenance"));
    }

    if($type === "abonne"){
        $menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("simulation", "Simulation"));
    }
    
	$menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("deconnexion", "Déconnexion"));
}
else{
	$menuPrincipalVlib->ajouterComposant($menuPrincipalVlib->creerItemLien("connexion", "Connexion"));
}
$menuPrincipalVlib = $menuPrincipalVlib->creerMenu($_SESSION['menuPrincipalVlib'], 'menuPrincipalVlib');

include_once dispatcher::dispatch($_SESSION['menuPrincipalVlib']);