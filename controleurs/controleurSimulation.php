<?php
$dateJour = date('Y-m-d');
$dateDebAbo = $_SESSION['infoAbo']['dateDebAbo'];
$dateFinAbo = $_SESSION['infoAbo']['dateFinAbo'];
$simulationEmprunt = new Formulaire('post', 'index.php', 'connexion', 'connexion');

$simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('nomUtil', "Nom : " . $_SESSION['infoAbo']['nom']), 1);
$simulationEmprunt->ajouterComposantTab();

$simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('prenomUtil', "Prénom : " . $_SESSION['infoAbo']['prenom']), 1);
$simulationEmprunt->ajouterComposantTab();

$simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('codeAcces', "Code d'accès : " . $_SESSION['infoAbo']['codeAcces']), 1);
$simulationEmprunt->ajouterComposantTab();

$simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('dateFin', "Date de fin d'abonnement : " . $_SESSION['infoAbo']['dateFinAbo']), 1);
$simulationEmprunt->ajouterComposantTab();

if($dateJour > $dateDebAbo && $dateJour < $dateFinAbo){
    $lesStations = new Station();
    $station = StationDAO::lesStationsFonctionnelles();
    for ($i = 0; $i < count($station); $i++){
        $stations[] = $station[$i]->getNumS();
    }

    $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('validiteEmprunt', 'Abonnement valide'), 1);
    $simulationEmprunt->ajouterComposantTab();

    $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('numStation', 'Séléctionnez une station : '), 1);
    $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerSelect('lesStations', 'lesStations', $stations, ''));
    $simulationEmprunt->ajouterComposantTab();

    if(!isset($_POST["choixStation"])){
        $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerInputSubmit('choixStation', 'choixStation', 'Valider'),2);
        $simulationEmprunt->ajouterComposantTab();
    }
    if(isset($_POST["choixStation"])){
        $numStation = $_POST['lesStations'];
        $lesPlots = new Plot();
        $plot = PlotDAO::plotStation($lesPlots, $numStation);
        for ($i = 0; $i < count($plot); $i++){
            $plots[] = $plot[$i]->getNumP();
        }

        $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('numStation', 'Séléctionnez un plot : '), 1);
        $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerSelect('lesPlots', 'lesPlots', $plots, ''));
        $simulationEmprunt->ajouterComposantTab();
    }
}
else{
    $simulationEmprunt->ajouterComposantLigne($simulationEmprunt->creerLabelFor('validiteEmprunt', 'Abonnement invalide'), 1);
    $simulationEmprunt->ajouterComposantTab();
}

$formulaireEmprunt = $simulationEmprunt->creerFormulaire();

include_once 'vues/vueSimulation.php';