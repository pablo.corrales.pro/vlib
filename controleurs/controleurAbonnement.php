<?php
//Remplissage de tableau contenant les libellés des abonnements
$resultat = AbonnementDAO::lesAbonnements();
for ($i = 0; $i < count($resultat) - 1; $i++) {
    $unAbonnement = $resultat[$i]->getLibelleAbo();
    $tablo[] = $unAbonnement;
}
//Création du formulaire
$formulaireAbonnement = new Formulaire('post', 'index.php', 'fAbonnement', 'fAbonnement');
$formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Chacun de ces abonnements proposent un crédit de temps ainsi qu'un montant à débiter pour les 2 plus importants"));
$formulaireAbonnement->ajouterComposantTab();
//Partie choix de l'abonnement pour souscrire
$formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Type d'abonnement :"));
$formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerSelect('cbAbo', 'cbAbo', $tablo, ''));
$formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputSubmit('souscrire', 'souscrire', 'Souscrire'));
$formulaireAbonnement->ajouterComposantTab();
$typeA = $_POST['cbAbo'];
if(isset($typeA)){
    if($typeA == '0' || $typeA == '1'){
        //Partie des abonnements 24h et 7 jours
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Ahésion ponctuelle"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Numéro de téléphone ou numéro de téléphone :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputTexte('telOuMail', 'telOuMail', '', '', '', ''));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Choix du code secret à 4 chiffres :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputMdp('codeSecret', 'codeSecret', 0, '', ''),1);
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Conformation du code secret :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputMdp('confCodeSecret', 'confCodeSecret', 0, '', ''),1);
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputSubmit('validerPonct', 'validerPonct', 'Valider'));
        $formulaireAbonnement->ajouterComposantTab();

    }
    elseif($typeA =='2'|| $typeA == '3'){
        //Partie des abonnements 1 mois et 1 an
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Abonnement"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Nom :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputTexte('nom', 'nom', '', '', '', ''));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Prénom :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputTexte('prenom', 'prenom', '', '', '', ''));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Choix du code secret à 4 chiffres :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputMdp('codeSecret', 'codeSecret', 0, '', ''),1);
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Conformation du code secret :"));
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputMdp('confCodeSecret', 'confCodeSecret', 0, '', ''),1);
        $formulaireAbonnement->ajouterComposantTab();
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerInputSubmit('validerAbo', 'validerAbo', 'Valider'));
        $formulaireAbonnement->ajouterComposantTab();
    }
}
if(isset($_POST['validerPonct'])){
    if($_POST['codeSecret']==$_POST['confCodeSecret']){
        //Affecte toute les valeurs à un objet Abonne
        $newAbonne = new Abonne(strval(rand(100000, 999999)), $_POST['codeSecret']);
        $newAbonne->setNom($_POST['telOuMail']);
        //Date actuelle
        $today = date("Y-m-d");
        $newAbonne->setDateDebAbo($today);
        if($typeA == 0){
            $newAbonne->setCodeAbo('1');
            //Date du jour suivant
            $nextday  = mktime(0, 0, 0, date("Y"),   date("m"),   date("d")+1);
            $newAbonne->setDateFinAbo($nextday);
        }
        elseif($typeA == 1){
            $newAbonne->setCodeAbo('2');
            //Date de la semaine suivant
            $nextweak  = mktime(0, 0, 0, date("Y"),   date("m"),   date("d")+7);
            $newAbonne->setDateFinAbo($nextweak);
        }
        $newAbonne->setTypeUtil('usager');
        $newAbonne->setCreditTemps('00:00:30');
        $newAbonne->setMontantADebiter(NULL);
        AbonneDAO::creerAboType1($newAbonne);
    }
    else{
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Les codes secrets ne sont pas identiques"));
        $formulaireAbonnement->ajouterComposantTab();
    }
}
if(isset($_POST['validerAbo'])){
    if($_POST['codeSecret']==$_POST['confCodeSecret']){
        //Affecte toute les valeurs à un objet Abonne
        $newAbonne = new Abonne(rand(100000, 999999), $_POST['codeSecret']);
        $newAbonne->setNom($_POST['nom']);
        $newAbonne->setPrenom($_POST['prenom']);
        //Date actuelle
        $today = date("Y-m-d");
        $newAbonne->setDateDebAbo($today);
        if($typeA == 2){
            $newAbonne->setCodeAbo('3');
            //Date du moins suivant
            $nextmonth  = mktime(0, 0, 0, date("Y"),   date("m")+1,   date("d"));
            $newAbonne->setDateFinAbo($nextmonth);
        }
        elseif($typeA == 3){
            $newAbonne->setCodeAbo('4');
            //Date de l'année suivant
            $nextyear  = mktime(0, 0, 0, date("Y")+1,   date("m"),   date("d"));
            $newAbonne->setDateFinAbo($nextyear);
        }
        $newAbonne->setTypeUtil('abonne');
        $newAbonne->setCreditTemps('00:30:00');
        $newAbonne->setMontantADebiter('2.00');
        AbonneDAO::creerAboType2($newAbonne);
    }
    else{
        $formulaireAbonnement->ajouterComposantLigne($formulaireAbonnement->creerLabel("Les codes secrets ne sont pas identiques"));
        $formulaireAbonnement->ajouterComposantTab();
    }
}
$formulaireAbonnement->creerFormulaire();
include_once 'vues/vueAbonnement.php';
?>