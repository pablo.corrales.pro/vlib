<?php
if(!isset($_SESSION['resultatVerif']) || !$_SESSION['resultatVerif']){
	$formulaireConnexion = new Formulaire('post', 'index.php', 'connexion', 'connexion');

	$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerLabelFor('codeAcces', "Code d'accés :"), 1);
	$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputTexte('codeAcces', 'codeAcces', ''   , 1, '',0),1);
	$formulaireConnexion->ajouterComposantTab();
	
	$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerLabelFor('codeSecret', 'Code secret :'), 1);
	$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputMdp('codeSecret', 'codeSecret', '', 0, 0),1);
	$formulaireConnexion->ajouterComposantTab();
	
	$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerInputSubmit('submitConnex', 'submitConnex', 'Valider'),2);
	$formulaireConnexion->ajouterComposantTab();
	
	$formulaireConnexion->ajouterComposantLigne($formulaireConnexion->creerMessage($messageErreurConnexion, "messageErreurConnexion"),2);
	$formulaireConnexion->ajouterComposantTab();

	$leformulaireConnex = $formulaireConnexion->creerFormulaire();
	
	include_once 'vues/vueConnexion.php';
}
else{
	die();
	$_SESSION['resultatVerif']=[];
	$_SESSION['menuPrincipalVlib']="accueil";
	header('location: index.php');
}