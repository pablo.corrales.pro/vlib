<?php
/*******************************************
 * Création d'un menu
 *******************************************/
if(isset($_GET['menuMaintenance'])){
	$_SESSION['menuMaintenance']= $_GET['menuMaintenance'];
}
else
{
	if(!isset($_SESSION['menuMaintenance'])){
		$_SESSION['menuMaintenance']="acceuil";
	}
}

$menuMaintenance = new Menu("menuMaintenance");

$menuMaintenance->ajouterComposant($menuMaintenance->creerItemLien("station", "Station"));
$menuMaintenance->ajouterComposant($menuMaintenance->creerItemLien("plot", "Plot"));
$menuMaintenance->ajouterComposant($menuMaintenance->creerItemLien("velo", "Vélo"));

$leMenuMaintenance = $menuMaintenance->creerMenu($_SESSION['menuMaintenance'], 'menuMaintenance');

/*****************************************************************************************************
 * Récupérer l'item sélectionnée
 *****************************************************************************************************/
$itemActif = $_SESSION['menuMaintenance'];

/*****************************************************************************************************
 * Formulaire station
 *****************************************************************************************************/
if($itemActif == "station"){
    $formMaintenance = new Formulaire('POST', 'index.php', 'formMaintenance', '');

    /*******************************************
    * La liste de tous les stations
     *******************************************/
    $listeStations = StationDAO::lesStations();
    $tabStation = new Tableau(1, $listeStations);

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("numS", "Numéro de station : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputNumber("numS", "numS", "", 1, 100, "Nombre de la 1ère colonne"), 1);
    $formMaintenance->ajouterComposantTab();

    /**************************************
    * Autres éléments formulaire
    **************************************/

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("dateHeure", "La date : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputDate("dateHeure", "dateHeure", ""), 1);

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("duree", "La duree : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputTime("duree", "duree", "", 0, 100), 1);
    $formMaintenance->ajouterComposantTab();

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("maintenance", "maintenance", "Maintenance"), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("enFonction", "enFonction", "En Fonction"), 1);
    $formMaintenance->ajouterComposantTab();

    $leformMaintenanceS = $formMaintenance->creerFormulaire();
}

/*****************************************************************************************************
 * Formulaire plot
 *****************************************************************************************************/
if($itemActif == 'plot'){
    /*****************************************************************************************************
    * Formulaire station
    *****************************************************************************************************/
    $formMaintenance = new Formulaire('POST', 'index.php', 'formMaintenance', '');

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("numS", "Numéro de station : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputNumber("numS", "numS", "", 1, 100, "Nombre de la 1ère colonne"), 1);
    $formMaintenance->ajouterComposantTab();

    /***************************************
    * La liste de tous les plots
    ***************************************/
    $listePlots = PlotDAO::lesPlots();
    $tabPlot = new Tableau(2, $listePlots);

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("numP", "Numéro de plot : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputNumber("numP", "numP", "", 1, 100, "Nombre de la 2ème colonne"), 1);
    $formMaintenance->ajouterComposantTab();

    /**************************************
    * Autres éléments formulaire
    **************************************/

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("dateHeure", "La date : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputDate("dateHeure", "dateHeure", ""), 1);

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("duree", "La duree : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputTime("duree", "duree", "", 0, 100), 1);
    $formMaintenance->ajouterComposantTab();

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("maintenance", "maintenance", "Maintenance"), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("enFonction", "enFonction", "En Fonction"), 1);
    $formMaintenance->ajouterComposantTab();

    $leformMaintenanceP = $formMaintenance->creerFormulaire();
}

/*****************************************************************************************************
 * Formulaire velo
 *****************************************************************************************************/
if($itemActif == 'velo'){
    /*****************************************************************************************************
    * Formulaire station
    *****************************************************************************************************/
    $formMaintenance = new Formulaire('POST', 'index.php', 'formMaintenance', '');

    /**************************************
    * La liste de tous les vélos
    **************************************/
    $listeVelos = VeloDAO::lesVelos();
    $tabVelo = new Tableau(3, $listeVelos);

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("numV", "Numéro du velo : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputNumber("numV", "numV", "", 1, 100, "Nombre de la 1ère colonne"), 1);
    $formMaintenance->ajouterComposantTab();

    /**************************************
    * Autres éléments formulaire
    **************************************/

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("dateHeure", "La date : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputDate("dateHeure", "dateHeure", ""), 1);

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerLabelFor("duree", "La duree : "), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputTime("duree", "duree", "", 0, 100), 1);
    $formMaintenance->ajouterComposantTab();

    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("maintenance", "maintenance", "Maintenance"), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("enFonction", "enFonction", "En Fonction"), 1);
    $formMaintenance->ajouterComposantLigne($formMaintenance->creerInputSubmit("perdu", "perdu", "Perdu"), 1);
    $formMaintenance->ajouterComposantTab();

    $leformMaintenanceV = $formMaintenance->creerFormulaire();
}

if (!empty($_POST['dateHeure']) && !empty($_POST['duree'])){
    /********************************************************
    * Mise en maintenance ou mise en fonction
    ********************************************************/

    /********************************************************
    * Pour les stations
    ********************************************************/
    if($itemActif == "station"){
        if(isset($_POST['maintenance'])){
            //Création d'un nouvel objet station et etatStation
            if(!empty($_POST['numS'])){
                $station = new Station();
                $station->setNumS($_POST['numS']);

                $etatStation = new EtatStation();
                $etatStation->setDateHeure($_POST['dateHeure']);
                $etatStation->setDuree($_POST['duree']);
                $etatStation->setNumS($_POST['numS']);
                $etatStation->setEtatS('maintenance');

                StationDAO::stationEnMaintenance($station);
                EtatStationDAO::stationEnMaintenance2($etatStation);

                header("Refresh:0");
            }
        }

        if(isset($_POST['enFonction'])){
            //Création d'un nouvel objet station et etatStation
            if(!empty($_POST['numS'])){
                $station = new Station();
                $station->setNumS($_POST['numS']);

                $etatStation = new EtatStation();
                $etatStation->setDateHeure($_POST['dateHeure']);
                $etatStation->setDuree($_POST['duree']);
                $etatStation->setNumS($_POST['numS']);
                $etatStation->setEtatS('fonctionnel');
                
                StationDAO::stationFonctionnel($station);
                EtatStationDAO::stationFonctionnel2($etatStation);

                header("Refresh:0");
            }
        }
    }

    /********************************************************
    * Pour les plots
    ********************************************************/
    if($itemActif == "plot"){
        if(isset($_POST['maintenance'])){
            if(!empty($_POST['numS']) && !empty($_POST['numP'])){ 
                //Création d'un nouvel objet station, plot etatPlot et etatStation   
                $plot = new Plot();
                $plot->setNumP($_POST['numP']);
                $plot->setNumS($_POST['numS']);

                $etatPlot = new EtatPlot();
                $etatPlot->setDateHeure($_POST['dateHeure']);
                $etatPlot->setDuree($_POST['duree']);
                $etatPlot->setNumS($_POST['numS']);
                $etatPlot->setNumP($_POST['numP']);  
                $etatPlot->setEtatP('maintenance'); 

                PlotDAO::plotEnMaintenance($plot);
                EtatPlotDAO::plotEnMaintenance2($etatPlot);

                header("Refresh:0");
            }
        }

        if(isset($_POST['enFonction'])){
            if(!empty($_POST['numS']) && !empty($_POST['numP'])){
                //Création d'un nouvel objet station, plot etatPlot et etatStation
                $plot = new Plot();
                $plot->setNumP($_POST['numP']);
                $plot->setNumS($_POST['numS']);

                $etatPlot = new EtatPlot();
                $etatPlot->setDateHeure($_POST['dateHeure']);
                $etatPlot->setDuree($_POST['duree']);
                $etatPlot->setNumS($_POST['numS']);
                $etatPlot->setNumP($_POST['numP']);  
                $etatPlot->setEtatP('fonctionnel'); 

                PlotDAO::plotFonctionnel($plot);
                EtatPlotDAO::plotFonctionnel2($etatPlot);

                header("Refresh:0");
            }
        }
    }

    /********************************************************
    * Pour les vélo
    ********************************************************/
    if($itemActif == "velo"){
        if(isset($_POST['maintenance'])){
            //Création d'un nouvel objet velo et etatVelo
            if(!empty($_POST['numV'])){
                $velo = new Velo();
                $velo->setNumV($_POST['numV']);

                $etatVelo = new EtatVelo();
                $etatVelo->setDateHeure($_POST['dateHeure']);
                $etatVelo->setDuree($_POST['duree']);
                $etatVelo->setNumV($_POST['numV']);
                $etatVelo->setEtatP('maintenance');

                VeloDAO::veloEnMaintenance($velo);
                EtatVeloDAO::veloEnMaintenance2($etatVelo);

                header("Refresh:0");
            }
        }

        if(isset($_POST['enFonction'])){
            //Création d'un nouvel objet velo et etatVelo
            if(!empty($_POST['numV'])){
                $velo = new Velo();
                $velo->setNumV($_POST['numV']);
                
                $etatVelo = new EtatVelo();
                $etatVelo->setDateHeure($_POST['dateHeure']);
                $etatVelo->setDuree($_POST['duree']);
                $etatVelo->setNumV($_POST['numV']);
                $etatVelo->setEtatP('fonctionnel');

                VeloDAO::veloFonctionnel($velo);
                EtatVeloDAO::veloFonctionnel2($etatVelo);

                header("Refresh:0");
            }
        }

        if(isset($_POST['perdu'])){
            if(!empty($_POST['numV'])){
                //Création d'un nouvel objet velo et etatVelo
                $numV = $_POST['numV'];
                $velo = new Velo();
                $velo->setNumV($numV);

                $etatVelo = new EtatVelo();
                $etatVelo->setDateHeure($_POST['dateHeure']);
                $etatVelo->setDuree($_POST['duree']);
                $etatVelo->setNumV($_POST['numV']);
                $etatVelo->setEtatP('perdu');

                VeloDAO::veloPerdu($velo);
                EtatVeloDAO::veloPerdu2($etatVelo);

                header("Refresh:0");
            }
        }
    }
}

include_once 'vues/maintenance/vueMaintenance.php';
?>