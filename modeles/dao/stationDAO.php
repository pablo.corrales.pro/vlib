<?php
class StationDAO{
    public static function lesStations(){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from station"); 

        $requetePrepa->execute();
        
        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        return $reponse;
    }

    public static function lesStationsFonctionnelles(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from station where etatS = 'fonctionnel'");
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);
    
        if(!empty($liste)){
            foreach($liste as $station){
                $uneStation = new Station();
                $uneStation->hydrate($station);
                $result[] = $uneStation;
            }
        }
        return $result;
    }

    public static function stationEnMaintenance(Station $station){
        $requetePrepa2 = DBConnex::getInstance()->prepare("update station set etatS = 'en maintenance' where numS=:numS");
        $numS = $station->getNumS();
        $requetePrepa2->bindParam(":numS", $numS);
        $requetePrepa2->execute();

    }

    public static function stationFonctionnel(Station $station){
        $requetePrepa = DBConnex::getInstance()->prepare("update station set etatS = 'fonctionnel' where numS=:numS");
        $numS = $station->getNumS();
        $requetePrepa->bindParam(":numS", $numS);
        $requetePrepa->execute();

    }
}