<?php
class EtatPlotDAO{
    public static function plotEnMaintenance2(EtatPlot $etatPlot){

        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatplot (dateHeure, numS, numP, etatP, duree) values(:dateHeure, :numS, :numP, :etatP, :duree)");
        $dateHeure = $etatPlot->getDateHeure();
        $numS = $etatPlot->getNumS();
        $numP = $etatPlot->getNumP();
        $etatP = $etatPlot->getEtatP();
        $duree = $etatPlot->getDuree();

        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numS", $numS);
        $requetePrepa3->bindParam(":numP", $numP);
        $requetePrepa3->bindParam(":etatP", $etatP);
        $requetePrepa3->bindParam(":duree", $duree);

        return $requetePrepa3->execute();

    }

    public static function plotFonctionnel2(EtatPlot $etatPlot){
        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatplot (dateHeure, numS, numP, etatP, duree) values(:dateHeure, :numS, :numP, :etatP, :duree)");
        $dateHeure = $etatPlot->getDateHeure();
        $numS = $etatPlot->getNumS();
        $numP = $etatPlot->getNumP();
        $etatP = $etatPlot->getEtatP();
        $duree = $etatPlot->getDuree();

        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numS", $numS);
        $requetePrepa3->bindParam(":numP", $numP);
        $requetePrepa3->bindParam(":etatP", $etatP);
        $requetePrepa3->bindParam(":duree", $duree);

        return $requetePrepa3->execute();
    }
}