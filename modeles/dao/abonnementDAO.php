<?php
class AbonnementDAO{
    public static function lesAbonnements(){
        $result =[];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from abonnement");
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($liste)){
            foreach($liste as $abonnement){
                $unAbonnement = new Abonnement();
                $unAbonnement->hydrate($abonnement);
                $result[] = $unAbonnement;
            }
        }
        return $result;
    }

    public static function getAbonnement($type){
        $result = "";
        $requetePrepa = DBConnex::getInstance()->prepare("select * from abonnement where typeA = :type");
        $requetePrepa->bindParam(':type', $type);
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($liste)){
            foreach($liste as $abonnement){
                $unAbonnement = new Abonnement();
                $unAbonnement->hydrate($abonnement);
                $result = $unAbonnement;
            }
        }
        return $result;
    }
}