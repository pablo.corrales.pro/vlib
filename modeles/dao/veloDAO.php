<?php
class VeloDAO{
    public static function lesVelos(){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from velo"); 

        $requetePrepa->execute();
        
        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        return $reponse;
    }

    public static function veloEnMaintenance(Velo $velo){
        $requetePrepa = DBConnex::getInstance()->prepare("update velo set etatV = 'en maintenance' where numV=:numV");
        $numV = $velo->getNumV();
        $requetePrepa->bindParam(":numV", $numV);
        $requetePrepa->execute();

    }

    public static function veloFonctionnel(Velo $velo){
        $requetePrepa = DBConnex::getInstance()->prepare("update velo set etatV = 'fonctionnel' where numV=:numV");
        $numV = $velo->getNumV();
        $requetePrepa->bindParam(":numV", $numV);
        $requetePrepa->execute();
    }

    public static function veloPerdu(Velo $velo){
        $requetePrepa = DBConnex::getInstance()->prepare("update velo set etatV = 'perdu' where numV=:numV");
        $numV = $velo->getNumV();
        $requetePrepa->bindParam(":numV", $numV);
        $requetePrepa->execute();
    }
}