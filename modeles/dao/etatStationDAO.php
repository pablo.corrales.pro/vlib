<?php
class EtatStationDAO{
    public static function stationEnMaintenance2(EtatStation $etatStation){
        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatstation (dateHeure, numS, etatS, duree) values(:dateHeure, :numS, :etatS, :duree)");
        $dateHeure = $etatStation->getDateHeure();
        $numS = $etatStation->getNumS();
        $etatS = $etatStation->getEtatS();
        $duree = $etatStation->getDuree();
        
        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numS", $numS);
        $requetePrepa3->bindParam(":etatS", $etatS);
        $requetePrepa3->bindParam(":duree", $duree);
        
        return $requetePrepa3->execute();
        
    }

    public static function stationFonctionnel2(EtatStation $etatStation){
        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatstation (dateHeure, numS, etatS, duree) values(:dateHeure, :numS, :etatS, :duree)");
        $dateHeure = $etatStation->getDateHeure();
        $numS = $etatStation->getNumS();
        $etatS = $etatStation->getEtatS();
        $duree = $etatStation->getDuree();

        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numS", $numS);
        $requetePrepa3->bindParam(":etatS", $etatS);
        $requetePrepa3->bindParam(":duree", $duree);
        
        return $requetePrepa3->execute();
    }
}