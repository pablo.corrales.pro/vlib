<?php
class AbonneDAO{
    public static function verification(Abonne $abonne){
        
        $requetePrepa = DBConnex::getInstance()->prepare("select codeAcces from abonne where codeAcces = :codeAcces and  codeSecret = :codeSecret");
        $codeAcces = $abonne->getCodeAcces();
        $codeSecret =  $abonne->getCodeSecret();
        
        $requetePrepa->bindParam( ":codeAcces", $codeAcces);
        $requetePrepa->bindParam( ":codeSecret", $codeSecret);   

        $requetePrepa->execute();
        
        return $requetePrepa->fetch(PDO::FETCH_ASSOC);
    }

    public static function infoAbo(Abonne $abonne){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from abonne where codeAcces = :codeAcces");
        $codeAcces = $abonne->getCodeAcces();

        $requetePrepa->bindParam(":codeAcces", $codeAcces);
        $requetePrepa->execute();

        return $requetePrepa->fetch(PDO::FETCH_ASSOC);
    }

    public static function creerAboType1(Abonne $unAbo){
        $requetePrepa = DBConnex::getInstance()->prepare("insert into abonne (codeAcces, codeSecret, codeAbo, nom, dateDebAbo, creditTemps, montantADebiter, typeUtil) values (:codeAcces, :codeSecret, :codeAbo, :nom, :dateDebAbo, :creditTemps, :montantADebiter, :typeUtil)");
        $codeAcces = $unAbo->getCodeAcces();
        $codeSecret = $unAbo->getCodeSecret();
        $codeAbo = $unAbo->getCodeAbo();
        $nom = $unAbo->getNom();
        $dateDebAbo = $unAbo->getDateDebAbo();
        $creditTemps = $unAbo->getCreditTemps();
        $montantDebit = $unAbo->getMontantADebiter();
        $typeUtil = $unAbo->getTypeUtil();
        
        $requetePrepa()->bindParam(":codeAcces", $codeAcces);
        $requetePrepa()->bindParam(":codeSecret", $codeSecret);
        $requetePrepa()->bindParam(":codeAbo", $codeAbo);
        $requetePrepa()->bindParam(":nom", $nom);
        $requetePrepa()->bindParam(":dateDebAbo", $dateDebAbo);
        $requetePrepa()->bindParam(":creditTemps", $creditTemps);
        $requetePrepa()->bindParam(":montantADebiter", $montantADebiter);
        $requetePrepa()->bindParam(":typeUtil", $typeUtil);
        $requetePrepa->execute();

        $creerAbo = $requetePrepa->fetch();
        return $creerAbo;
    }

    public static function creerAboType2(Abonne $unAbo){
        $requetePrepa = DBConnex::getInstance()->prepare("insert into abonne (codeAcces, codeSecret, codeAbo, nom, prenom, dateDebAbo, creditTemps, montantADebiter, typeUtil) values (:codeAcces, :codeSecret, :codeAbo, :nom, :prenom, :dateDebAbo, :creditTemps, :montantADebiter, :typeUtil)");
        $codeAcces = $unAbo->getCodeAcces();
        $codeSecret = $unAbo->getCodeSecret();
        $codeAbo = $unAbo->getCodeAbo();
        $nom = $unAbo->getNom();
        $prenom = $unAbo->getPrenom();
        $dateDebAbo = $unAbo->getDateDebAbo();
        $creditTemps = $unAbo->getCreditTemps();
        $montantDebit = $unAbo->getMontantADebiter();
        $typeUtil = $unAbo->getTypeUtil();
        var_dump($codeAcces);
        
        $requetePrepa()->bindParam(":codeAcces", $codeAcces);
        $requetePrepa()->bindParam(":codeSecret", $codeSecret);
        $requetePrepa()->bindParam(":codeAbo", $codeAbo);
        $requetePrepa()->bindParam(":nom", $nom);
        $requetePrepa()->bindParam(":prenom", $prenom);
        $requetePrepa()->bindParam(":dateDebAbo", $dateDebAbo);
        $requetePrepa()->bindParam(":creditTemps", $creditTemps);
        $requetePrepa()->bindParam(":montantADebiter", $montantADebiter);
        $requetePrepa()->bindParam(":typeUtil", $typeUtil);
        $requetePrepa->execute();

        $creerAbo = $requetePrepa->fetch();
        return $creerAbo;
    }
}