<?php
class PlotDAO{
    public static function lesPlots(){
        $requetePrepa = DBConnex::getInstance()->prepare("select * from plot"); 
        $requetePrepa->execute();
        
        $reponse = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        return $reponse;
    }

    public static function plotEnMaintenance(Plot $plot){
        $requetePrepa = DBConnex::getInstance()->prepare("update plot set etat = 'en maintenance' where numS=:numS and numP=:numP");
        $numS = $plot->getNumS();
        $numP = $plot->getNumP();
        $requetePrepa->bindParam(":numS", $numS);
        $requetePrepa->bindParam(":numP", $numP);
        return $requetePrepa->execute();
    }

    public static function plotFonctionnel(Plot $plot){
        $requetePrepa = DBConnex::getInstance()->prepare("update plot set etat = 'fonctionnel' where numS=:numS and numP=:numP");
        $numS = $plot->getNumS();
        $numP = $plot->getNumP();
        $requetePrepa->bindParam(":numS", $numS);
        $requetePrepa->bindParam(":numP", $numP);
        return $requetePrepa->execute();
    }

    public static function plotStation(Plot $plot, $numS){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from plot where etat = 'fonctionnel' and numS = :numS");
        $requetePrepa->bindParam(":numS", $numS);
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($liste)){
            foreach($liste as $station){
                $uneStation = new Station();
                $uneStation->hydrate($station);
                $result[] = $uneStation;
            }
        }
        return $result;
    }
}