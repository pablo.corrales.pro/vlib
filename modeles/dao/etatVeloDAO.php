<?php
class EtatVeloDAO{
    public static function veloEnMaintenance2(EtatVelo $etatVelo){
        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatvelo (dateHeure, numV, etatP, duree) values(:dateHeure, :numV, :etatP, :duree)");
        $dateHeure = $etatVelo->getDateHeure();
        $numV = $etatVelo->getNumV();
        $etatP = $etatVelo->getEtatP();
        $duree = $etatVelo->getDuree();

        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numV", $numV);
        $requetePrepa3->bindParam(":etatP", $etatP);
        $requetePrepa3->bindParam(":duree", $duree);

        $requetePrepa3->execute();


    }

    public static function veloFonctionnel2(EtatVelo $etatVelo){
        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatvelo (dateHeure, numV, etatP, duree) values(:dateHeure, :numV, :etatP, :duree)");
        $dateHeure = $etatVelo->getDateHeure();
        $numV = $etatVelo->getNumV();
        $etatV = $etatVelo->getEtatP();
        $duree = $etatVelo->getDuree();

        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numV", $numV);
        $requetePrepa3->bindParam(":etatP", $etatP);
        $requetePrepa3->bindParam(":duree", $duree);

        return $requetePrepa3->execute();
    }

    public static function veloPerdu2(EtatVelo $etatVelo){
        $requetePrepa3 = DBConnex::getInstance()->prepare("insert into etatvelo (dateHeure, numV, etatP, duree) values(:dateHeure, :numV, :etatP, :duree)");
        $dateHeure = $etatVelo->getDateHeure();
        $numV = $etatVelo->getNumV();
        $etatP = $etatVelo->getEtatP();
        $duree = $etatVelo->getDuree();

        $requetePrepa3->bindParam(":dateHeure", $dateHeure);
        $requetePrepa3->bindParam(":numV", $numV);
        $requetePrepa3->bindParam(":etatP", $etatP);
        $requetePrepa3->bindParam(":duree", $duree);

        return $requetePrepa3->execute();
    }
}