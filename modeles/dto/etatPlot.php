<?php
class EtatPlot{
    private $dateHeure;
    private $numS;
    private $numP;
    private $etatP;
    private $duree;

    public function _construct($uneDateHeure = NULL, $unNumS = NULL, $unNumP = NULL, $unEtatP = NULL, $uneDuree = NULL){
        $this->dateHeure = $uneDateHeure;
        $this->numS = $unNumS;
        $this->numP = $unNumP;
        $this->etatP = $unEtatP;
        $this->duree = $unEtat;
    }

    public function getDateHeure(){
        return $this->dateHeure;
    }

    public function setDateHeure($uneDateHeure){
        $this->dateHeure = $uneDateHeure;
    }

    public function getNumS(){
        return $this->numS;
    }

    public function setNumS($unNumS){
        $this->numS = $unNumS;
    }

    public function getNumP(){
        return $this->numP;
    }

    public function setNumP($unNumP){
        $this->numP = $unNumP;
    }

    public function getEtatP(){
        return $this->etatP;
    }

    public function setEtatP($unEtatP){
        $this->etatP = $unEtatP;
    }

    public function getDuree(){
        return $this->duree;
    }

    public function setDuree($uneDuree){
        $this->duree = $uneDuree;
    }
}