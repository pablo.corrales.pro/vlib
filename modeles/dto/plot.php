<?php
class Plot{
    private $numS;
    private $numP;
    private $numV;
    private $etat;

    public function _construct($unNumV = NULL, $unNumS = NULL, $unNumP = NULL){
        $this->numV = $unNumS;
        $this->numS = $unNumP;
        $this->numP = $unNumV;
    }

    public function getNumS(){
        return $this->numS;
    }

    public function setNumS($unNumS){
        $this->numS = $unNumS;
    }

    public function getNumP(){
        return $this->numP;
    }

    public function setNumP($unNumP){
        $this->numP = $unNumP;
    }

    public function getNumV(){
        return $this->numV;
    }

    public function setNumV($unNumV){
        $this->numV = $unNumV;
    }

    public function getEtat(){
        return $this->etat;
    }

    public function setEtat($unEtat){
        $this->etat = $unEtat;
    }
}