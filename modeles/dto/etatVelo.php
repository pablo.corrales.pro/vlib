<?php
class EtatVelo{
    private $dateHeure;
    private $numV;
    private $etatP;
    private $duree;

    public function _construct($uneDateHeure = NULL, $unNumV = NULL, $unEtatP = NULL, $uneDuree = NULL){
        $this->dateHeure = $uneDateHeure;
        $this->numV = $unNumV;
        $this->etatP = $unEtatP;
        $this->duree = $unEtat;
    }

    public function getDateHeure(){
        return $this->dateHeure;
    }

    public function setDateHeure($uneDateHeure){
        $this->dateHeure = $uneDateHeure;
    }

    public function getNumV(){
        return $this->numV;
    }

    public function setNumV($unNumV){
        $this->numV = $unNumV;
    }

    public function getEtatP(){
        return $this->etatP;
    }

    public function setEtatP($unEtatP){
        $this->etatP = $unEtatP;
    }

    public function getDuree(){
        return $this->duree;
    }

    public function setDuree($uneDuree){
        $this->duree = $uneDuree;
    }
}