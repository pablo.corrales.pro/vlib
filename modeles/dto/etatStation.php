<?php
class EtatStation{
    private $dateHeure;
    private $numS;
    private $etatS;
    private $duree;

    public function _construct($uneDateHeure = NULL, $unNumS = NULL, $unEtatS = NULL, $uneDuree = NULL){
        $this->dateHeure = $uneDateHeure;
        $this->numS = $unNumS;
        $this->etatS = $unEtatS;
        $this->duree = $unEtat;
    }

    public function getDateHeure(){
        return $this->dateHeure;
    }

    public function setDateHeure($uneDateHeure){
        $this->dateHeure = $uneDateHeure;
    }

    public function getNumS(){
        return $this->numS;
    }

    public function setNumS($unNumS){
        $this->numS = $unNumS;
    }

    public function getEtatS(){
        return $this->etatS;
    }

    public function setEtatS($unEtatS){
        $this->etatS = $unEtatS;
    }

    public function getDuree(){
        return $this->duree;
    }

    public function setDuree($uneDuree){
        $this->duree = $uneDuree;
    }
}