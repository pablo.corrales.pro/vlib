<?php

Class Station{
    use Hydrate;
    private $numS;
    private $etatS;
    private $nomS;
    private $situationS;
    private $capaciteS;
    private $numBorne;

    public function __construct($unNumS = NULL){
        $this->numS = $unNumS;
    }

    public function getNumS(){
        return $this->numS;
    }

    public function setNumS($unNumS){
        $this->numS = $unNumS;
    }

    public function getEtatS(){
        return $this->etatS;
    }

    public function setEtatS($unEtatS){
        $this->etatS = $unEtatS;
    }

    public function getNomS(){
        return $this->nomS;
    }

    public function setNomS($unNomS){
        $this->nomS = $unNomS;
    }

    public function getSituationS(){
        return $this->situationS;
    }

    public function setSituationS($uneSituationS){
        $this->situationS = $uneSituationS;
    }

    public function getCapaciteS(){
        return $this->capaciteS;
    }

    public function setCapaciteS($uneCapaciteS){
        $this->capaciteS = $uneCapaciteS;
    }

    public function getNumBorne(){
        return $this->numBorne;
    }

    public function setNumBorne($unNumBorne){
        $this->numBorne = $unNumBorne;
    }
}