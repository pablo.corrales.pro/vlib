<?php
class Velo{
    private $numV;
    private $numS;
    private $numP;
    private $etatV;
    private $DMEC;

    public function _construct($unNumV = NULL, $unNumS = NULL, $unNumP = NULL){
        $this->numV = $unNumV;
        $this->numS = $unNumS;
        $this->numP = $unNumP;
    }

    public function getNumV(){
        return $this->numV;
    }

    public function setNumV($unNumV){
        $this->numV = $unNumV;
    }

    public function getNumS(){
        return $this->numS;
    }

    public function setNumS($unNumS){
        $this->numS = $unNumS;
    }

    public function getNumP(){
        return $this->numP;
    }

    public function setNumP($unNumP){
        $this->numP = $unNumP;
    }

    public function getEtatV(){
        return $this->etatV;
    }

    public function setEtatV($unEtat){
        $this->etatV = $unEtat;
    }

    public function getDMEC(){
        return $this->DMEC;
    }

    public function setDMEC($unDMEC){
        $this->DMEC = $unDMEC;
    }
}