<?php
class Abonnement{
    use Hydrate;
    private $codeAbo;
    private $libelleAbo;
    private $dureeAbo;
    private $montantAbo;
    private $creditTempsBase;
    private $tarifHoraire;
    private $caution;
    private $typeA;

    public function __construct($unCodeAbo = NULL, $unLibelleAbo = NULL){
        $this->codeAbo = $unCodeAbo;
        $this->libelleAbo = $unLibelleAbo;
    }

    public function getCodeAbo(){
        return $this->codeAbo;
    }

    public function setCodeAbo($unCodeAbo){
        $this->codeAbo = $unCodeAbo;
    }

    public function getLibelleAbo(){
        return $this->libelleAbo;
    }

    public function setLibelleAbo($unLibelleAbo){
        $this->libelleAbo = $unLibelleAbo;
    }

    public function getDureeAbo(){
        return $this->dureeAbo;
    }

    public function setDureeAbo($unDureeAbo){
        $this->dureeAbo = $unDureeAbo;
    }

    public function getMontantAbo(){
        return $this->montantAbo;
    }

    public function setMontantAbo($UnMontantAbo){
        $this->montantAbo = $UnMontantAbo;
    }

    public function getCreditTempsBase(){
        return $this->creditTempsBase;
    }

    public function setCreditTempsBase($unCreditTempsBase){
        $this->creditTempsBase = $unCreditTempsBase;
    }

    public function getTarifHoraire(){
        return $this->tarifHoraire;
    }

    public function setTarifHoraire($unTarifHoraire){
        $this->tarifHoraire = $unTarifHoraire;
    }

    public function getCaution(){
        return $this->caution;
    }

    public function setCaution($uneCaution){
        $this->caution = $uneCaution;
    }

    public function getTypeA(){
        return $this->typeA;
    }

    public function setTypeA($unTypeA){
        $this->typeA = $unTypeA;
    }
}