<?php
class Abonne{
    use Hydrate;
    private $codeAcces;
    private $codeSecret;
    private $codeAbo;
    private $nom;
    private $prenom;
    private $dateDebAbo;
    private $dateFinAbo;
    private $creditTemps;
    private $montantADebiter;
    private $typeUtil;

    public function __construct($unCodeAcces = NULL, $unCodeSecret = NULL){
        $this->codeAcces = $unCodeAcces;
        $this->codeSecret = $unCodeSecret;
    }

    public function getCodeAcces(){
        return $this->codeAcces;
    }

    public function setCodeAcces($unCodeAcces){
        $this->codeAcces = $unCodeAcces;
    }

    public function getCodeSecret(){
        return $this->codeSecret;
    }

    public function setCodeSecret($unCodeSecret){
        $this->codeSecret = $unCodeSecret;
    }

    public function getCodeAbo(){
        return $this->codeAbo;
    }

    public function setCodeAbo($unCodeAbo){
        $this->codeAbo = $unCodeAbo;
    }

    public function getNom(){
        return $this->nom;
    }

    public function setNom($unNom){
        $this->nom = $unNom;
    }

    public function getPrenom(){
        return $this->prenom;
    }

    public function setPrenom($unPrenom){
        $this->prenom = $unPrenom;
    }

    public function getDateDebAbo(){
        return $this->dateDebAbo;
    }

    public function setDateDebAbo($uneDateDebAbo){
        $this->dateDebAbo = $uneDateDebAbo;
    }

    public function getDateFinAbo(){
        return $this->dateFinAbo;
    }

    public function setDateFinAbo($uneDateFinAbo){
        $this->dateFinAbo = $uneDateFinAbo;
    }

    public function getCreditTemps(){
        return $this->creditTemps;
    }

    public function setCreditTemps($unCreditTemps){
        $this->creditTemps = $unCreditTemps;
    }

    public function getMontantADebiter(){
        return $this->montantADebiter;
    }

    public function setMontantADebiter($unMontantADebiter){
        $this->montantADebiter = $unMontantADebiter;
    }

    public function getTypeUtil(){
        return $this->typeUtil;
    }

    public function setTypeUtil($unTypeUtil){
        $this->typeUtil = $unTypeUtil;
    }
}